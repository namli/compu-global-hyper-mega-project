# TODO

## Next.js
- [x] Set up the project (Next.js)
- [x] Create index page
- [x] Add TypeScript, rewrite the index from JS
- [x] Add timestamps to index (current+generated), set revalidate
- [x] Show a list on index
- [x] Create detail page(s)

## Deployment
- [x] Add Vercel integration
- [x] Add Checkly check

## Nice to have
- [x] Add internationalization
- [x] Add translations (next-i18next)
- [ ] Install and show ChakraUI (@chakra-ui/react)
- [ ] Install and show GraphQL (@apollo/client)
