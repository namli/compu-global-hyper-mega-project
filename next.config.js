module.exports = {
    i18n: {
        locales: ['en', 'cs'],
        defaultLocale: 'en',
    },
    images: {
        domains: [
            'raw.githubusercontent.com'
        ]
    }
}
