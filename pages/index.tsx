import {FC} from 'react';
import NextLink from 'next/link';
import {getPokemon, Pokemon} from '../src/PokeApi';

const HomePage: FC<{ pokemon: Pokemon[] } > = ({ pokemon }) => {
    return <div>
        {pokemon.map((pokemon) => (
            <div key={`pokemon-${pokemon.name}`}>
                <NextLink href={`/pokemon/${pokemon.name}`}>
                    <a>{pokemon.name}</a>
                </NextLink>
            </div>
        ))}
    </div>
}

export default HomePage

export async function getStaticProps() {
    const result = await getPokemon(25);

    return {
        props: {
            pokemon: result.results
        }
    }
}
