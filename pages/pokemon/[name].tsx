import {FC} from 'react';
import Image from 'next/image';
import Link from 'next/link';
import {getPokemon, getPokemonByName, Pokemon} from '../../src/PokeApi';
import {serverSideTranslations} from 'next-i18next/serverSideTranslations';
import {useTranslation} from 'next-i18next';

const formatName = (name: string): string => {
    name = name.replaceAll('-', ' ');
    return name.charAt(0).toUpperCase() + name.slice(1);
}

const HomePage: FC<{ pokemon: Pokemon }> = ({ pokemon }) => {
    const { t } = useTranslation();

    return <div>
        <Link href="/">{t('pokemon-detail.back-button')}</Link>
        <h1>#{pokemon.order} {formatName(pokemon.name)}</h1>
        <Image src={pokemon.sprites.front_default} width="96" height="96" />
    </div>
}

export default HomePage

export async function getStaticProps({ params: { name }, locale }) {
    return {
        props: {
            pokemon: await getPokemonByName(name),
            ...(await serverSideTranslations(locale))
        }
    }
}

export async function getStaticPaths() {
    const result = await getPokemon(25)

    return {
        paths: [
            ...result.results.map((pokemon: Pokemon) => (
                { params: { name: pokemon.name }, locale: 'en' }
            )),
            ...result.results.map((pokemon: Pokemon) => (
                { params: { name: pokemon.name }, locale: 'cs' }
            )),
        ],
        fallback: false,
    }
}
