import {FC} from 'react';

const Greeting: FC<{ name: string }> = (props) => {
    return <h1>Hello {props.name}!</h1>
}

export default Greeting
