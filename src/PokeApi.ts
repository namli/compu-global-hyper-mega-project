const BASE_URL = 'https://pokeapi.co/api';

type ListResponse<T> = { count: number, results: T[] }
export type Pokemon = { name: string, order: number, sprites: { front_default: string } };

export const getPokemon = async (limit: number = 20, offset: number = 0): Promise<ListResponse<Pokemon>> => {
    const request = await fetch(`${BASE_URL}/v2/pokemon?offset=${offset}&limit=${limit}`);
    const { count, results } = await request.json();

    return { count, results };
}

export const getPokemonByName = async (name: string): Promise<Pokemon> => {
    const request = await fetch(`${BASE_URL}/v2/pokemon/${name}`);

    return await request.json();
}
